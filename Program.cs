﻿using HufmannRemastered.Controller;
using HufmannRemastered.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CommandLine;
using System.Text;
using System.Threading.Tasks;

namespace HufmannRemastered
{
    class Program
    {
        class Options
        {
            [Option('m', "mode", Required = true, HelpText = "k - code | d - decode")]
            public char Mode { get; set; }

            [Option('i', "input", Required = true, HelpText = "Input file to be processed.")]
            public string InputFile { get; set; }

            [Option('o', "output", Required = false, HelpText = "If specified, output is saved to file.")]
            public string OutputFile { get; set; }



        }
        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args)
              .WithParsed(opts => RunOptionsAndReturnExitCode(opts))
              .WithNotParsed((errs) => HandleParseError(errs));
        }

        private static void HandleParseError(IEnumerable<Error> errs)
        {
            Console.WriteLine("Bad parameters. Try again.");
        }

        private static void RunOptionsAndReturnExitCode(Options opts)
        {
            if (opts.Mode == 'k')
            {
                var fileToProcess = File.ReadAllBytes(opts.InputFile);
                var fileByChunks = Helpers.ChunkTrivialBetter(fileToProcess, 4096);

                List<Byte[]> chunksList = new List<byte[]>();
                foreach (var item in fileByChunks)
                {
                    var array = item.AsEnumerable().ToArray();
                    if (array.Length != 4096)
                    {
                        Array.Resize(ref array, 4096);
                    }
                    chunksList.Add(array);
                }

                int part = 0;
                string stdOut = string.Empty;
                List<HufmannCoder> coders = new List<HufmannCoder>();
                foreach (var item in chunksList)
                {
                    var coder = new HufmannCoder(item);
                    coders.Add(coder);
                    stdOut += string.Format("\n-----------------------------Part {0}-----------------------------\n", part);
                    stdOut += Helpers.LookupTable2String(coder.LookupTable);
                    stdOut += Helpers.ToBitString(coder.HufmannEncoded);
                    part++;
                }

                if (opts.OutputFile == null)
                {
                    Console.WriteLine(stdOut);
                }
                else
                {
                    //string OutputFilePathWithoutExtension = Path.GetFileNameWithoutExtension(opts.InputFile);
                    //string fileName = InputFilePathWithoutExtension + ".bin";
                    string fileName = opts.OutputFile;
                    if (File.Exists(fileName))
                    {
                        File.Delete(fileName);
                    }
                    foreach (var coder in coders)
                    {
                        coder.HufmannFileWriter(fileName, coder.HufmannEncoded, coder.LookupTable);
                    }
                }
            }
            if (opts.Mode == 'd')
            {
                var d = new HufmannDecoder(opts.InputFile);
                string stdOut = d.DecodeAll();
                if (opts.OutputFile == null)
                {
                    Console.WriteLine(stdOut);
                }
                else
                {
                    if (!File.Exists(opts.OutputFile))
                    {
                        File.WriteAllText(opts.OutputFile, stdOut);
                    }
                }
            }
        }
    }
}
//var v = File.ReadAllBytes(@"E:\TUL_ING\1\KAS\HufmannRemastered\TESTS\big.txt");
//var asd = Helpers.ChunkTrivialBetter(v, 4096);

//List<Byte[]> list = new List<byte[]>();
//            foreach (var item in asd)
//            {
//                var array = item.AsEnumerable().ToArray();
//                if(array.Length != 4096)
//                {
//                    Array.Resize(ref array, 4096);
//                }
//                list.Add(array);
//            }

//            int part = 0;
//string fileName = "test_compress.bin";
//            if (File.Exists(fileName))
//            {
//                File.Delete(fileName);
//            }
//            foreach (var item in list)
//            {
//                var coder = new HufmannCoder(item);
//Console.WriteLine(Helpers.LookupTable2String(coder.LookupTable));
//                Console.WriteLine(Helpers.ToBitString(coder.HufmannEncoded));
//                //string fileName = string.Format("test_compress{0}.bin", part);
//                FileOperations.HufmannFileWriter(fileName, coder.HufmannEncoded, coder.LookupTable);
//                part++;
//            }  
//            var d = new HufmannDecoder("test_compress.bin");
//Console.WriteLine(d.DecodeAll());
//            Console.ReadKey();