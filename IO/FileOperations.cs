﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HufmannRemastered.IO
{
    class FileOperations
    {
        public static char separator = '~';
        public static bool BinaryFileWrite(string fileName, BitArray[] arr)
        {
            int length = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                length += arr[i].Length;
            }
            BitArray bit_array = new BitArray(length);
            for(int i =0;i<arr.Length;i++)
            {
                for(int j =0;j<arr[i].Length;j++)
                {
                    var value = arr[i][j];
                    bit_array[0] = value;
                }

            }
            byte[] bytes = new byte[bit_array.Length / 8 + (bit_array.Length % 8 == 0 ? 0 : 1)];
            bit_array.CopyTo(bytes, 0);
            File.WriteAllBytes(fileName, bytes);
            return true;
        }
        //public static void HufmannFileWriter(string fileName, List<BitArray> encoded, Dictionary<char, BitArray> LookupTable)
        //{
        //    int length = 0;
        //    for (int i = 0; i < encoded.Count; i++)
        //    {
        //        length += encoded[i].Length;
        //    }
        //    try
        //    {
        //        using (BinaryWriter writer = new BinaryWriter(File.Open(fileName, FileMode.Append)))
        //        {
        //            foreach (KeyValuePair<char, BitArray> item in LookupTable)
        //            {
        //                var key = item.Key;
        //                var value = item.Value;
        //                writer.Write(Convert.ToByte(key));
        //                writer.Write(Convert.ToByte(value.Length));
        //                foreach (bool bit in value)
        //                {
        //                    writer.Write(bit);
        //                }
        //                writer.Flush();
        //            }
        //            writer.Write(Convert.ToByte(separator));
        //            writer.Flush();

        //            BitArray bit_array = new BitArray(length);
        //            var length_bytes = bit_array.Length / 8 + (bit_array.Length % 8 == 0 ? 0 : 1);

        //            writer.Write(length);
        //            writer.Flush();

        //            int position = 0;
        //            for (int i = 0; i < encoded.Count; i++)
        //            {
        //                for (int j = 0; j < encoded[i].Length; j++)
        //                {
        //                    bit_array[position] = encoded[i][j];
        //                    position++;
        //                }

        //            }
        //            byte[] bytes = new byte[length_bytes];
                    
        //            bit_array.CopyTo(bytes, 0);

        //            writer.Write(bytes);
        //            writer.Flush();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("Exception caught in process: {0}", ex);
        //    }
        //}
    }
}
