﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HufmannRemastered.IO
{
    public static class Helpers
    {
        public static char separator = '~';
        public static IEnumerable<IEnumerable<T>> ChunkTrivialBetter<T>(this IEnumerable<T> source, int chunksize)
        {
            var pos = 0;
            while (source.Skip(pos).Any())
            {
                yield return source.Skip(pos).Take(chunksize);
                pos += chunksize;
            }
        }
        public static string ToBitString(this BitArray bits)
        {
            var sb = new StringBuilder();

            for (int i = 0; i < bits.Count; i++)
            {
                char c = bits[i] ? '1' : '0';
                sb.Append(c);
            }

            return sb.ToString();
        }
        public static string ToBitString(List<BitArray> bits)
        {
            var sb = new StringBuilder();
            foreach(var b_array in bits)
            {
                for (int i = 0; i < b_array.Count; i++)
                {
                    char c = b_array[i] ? '1' : '0';
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }
        public static int BitsToByteSize(int a_size)
        {
            return a_size / 8 + (a_size % 8 == 0 ? 0 : 1);
        }
        public static string LookupTable2String(Dictionary<char, BitArray> LookupTable)
        {
            string output = string.Empty;
            output += "| KEY | | VALUE |\n";
            foreach (KeyValuePair<char, BitArray> _codes in LookupTable)
            {
                string row = string.Format("| {0} |  | {1} |\n", _codes.Key, ToBitString(_codes.Value));
                output += row;
            }
            return output;
        }
        public static byte ConvertToByte(BitArray bits)
        {
            if (bits.Count != 8)
            {
                throw new ArgumentException("bits");
            }
            byte[] bytes = new byte[1];
            bits.CopyTo(bytes, 0);
            return bytes[0];
        }
        public static byte[] BitArrayToBytes(BitArray bitarray)
        {
            if (bitarray.Length == 0)
            {
                throw new ArgumentException("must have at least length 1", "bitarray");
            }

            int num_bytes = bitarray.Length / 8;

            if (bitarray.Length % 8 != 0)
            {
                num_bytes += 1;
            }

            var bytes = new byte[num_bytes];
            bitarray.CopyTo(bytes, 0);
            return bytes;
        }
    }
}
