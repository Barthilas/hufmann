﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HufmannRemastered.IO
{
    class BitReader
    {
        int _bit;
        byte _currentByte;
        Stream _stream;
        public BitReader(Stream stream)
        { _stream = stream; }

        public bool? ReadBit(bool bigEndian = false)
        {
            if (_bit == 8)
            {

                var r = _stream.ReadByte();
                if (r == -1) return null;
                _bit = 0;
                _currentByte = (byte)r;
            }
            bool value;
            if (!bigEndian)
                value = (_currentByte & (1 << _bit)) > 0;
            else
                value = (_currentByte & (1 << (7 - _bit))) > 0;

            _bit++;
            return value;
        }
    }
}
