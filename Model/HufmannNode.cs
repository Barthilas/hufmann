﻿namespace Huffman.Model
{
    public class HufmannNode
    {
        public char Character
        {
            private set;
            get;
        }

        public int Frequency
        {
            private set;
            get;
        }
        public HufmannNode Left
        {
            private set;
            get;
        }

        public HufmannNode Right
        {
            private set;
            get;
        }

        public HufmannNode(char character, int frequency, HufmannNode left = null, HufmannNode right = null)
        {
            this.Character = character;
            this.Frequency = frequency;
            this.Left = left;
            this.Right = right;
        }

        public bool IsLeaf()
        {
            return Left == null && Right == null;
        }

        public static bool operator >=(HufmannNode a, HufmannNode b)
        {
            return a.Frequency >= b.Frequency;
        }

        public static bool operator <=(HufmannNode a, HufmannNode b)
        {
            return a.Frequency <= b.Frequency;
        }

        public static bool operator >(HufmannNode a, HufmannNode b)
        {
            return a.Frequency > b.Frequency;
        }

        public static bool operator <(HufmannNode a, HufmannNode b)
        {
            return a.Frequency < b.Frequency;
        }
    }
}