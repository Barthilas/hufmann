﻿using HufmannRemastered.IO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HufmannRemastered.Controller
{
    public class HufmannDecoder
    {
        public List<Dictionary<char, BitArray>> LookupTable { get; set; }

        public List<BitArray> HufmannEncoded { get; set; }
        public string HufmannDecoded { get; set; }

        public List<int> NumberOfBits { get; set; }

        public HufmannDecoder(string filename)
        {
            LookupTable = new List<Dictionary<char, BitArray>>();
            HufmannEncoded = new List<BitArray>();
            NumberOfBits = new List<int>();
            HufmannDecoded = string.Empty;
            HufmannBinaryReader(filename);
        }
        public string DecodeAll()
        {
            string overall = string.Empty;
            for(int i=0;i<HufmannEncoded.Count;i++)
            {
                overall += Decode(HufmannEncoded[i], LookupTable[i], NumberOfBits[i]);
            }
            return overall;
        }
        private string Decode(BitArray encoded, Dictionary<char,BitArray> codes, int n_bits)
        {
            string result = string.Empty;
            int readedBits = 0;
            List<bool> buffer = new List<bool>();
            foreach (bool bit in encoded)
            {
                buffer.Add(bit);
                foreach (KeyValuePair<char, BitArray> item in codes)
                {
                    bool isCorrect = false;
                    if (buffer.Count == item.Value.Length)
                    {
                        for (int i = 0; i < item.Value.Length; i++)
                        {
                            if (item.Value[i] != buffer[i])
                            {
                                isCorrect = false;
                                break;
                            }
                            isCorrect = true;
                        }
                    }

                    if (isCorrect)
                    {
                        result += item.Key;
                        readedBits += item.Value.Length;
                        buffer.Clear();
                        break;
                    }
                    //End of decoding, everything behind this is junk.
                    if(readedBits == n_bits)
                    {
                        goto hell;
                    }
                }
            }
            hell:
            return result;
        }
        private void HufmannBinaryReader(string filename)
        {
            List<BitArray> encoded = new List<BitArray>();
            int dataLenghtBytes = 0;
            int currentDataLenght = 0;
            bool creatingLookupTable = true;
            int tmp = 0;
            var dict = new Dictionary<char, BitArray>();

            try
            {

                using (var reader = new BinaryReader(File.Open(filename, FileMode.Open, FileAccess.Read)))
                {
                    while (reader.BaseStream.Position != reader.BaseStream.Length)
                    {
                        if (creatingLookupTable)
                        {
                            char character = reader.ReadChar();

                            if (character == Helpers.separator)
                            {
                                creatingLookupTable = false;
                                NumberOfBits.Add(reader.ReadInt32());
                                dataLenghtBytes = Helpers.BitsToByteSize(NumberOfBits[tmp]);
                                continue;
                            }

                            var len = reader.ReadByte();

                            bool[] code = new bool[len];
                            for (int i = 0; i < len; i++)
                            {
                                code[i] = reader.ReadBoolean();
                            }
                            dict.Add(character, new BitArray(code));
                        }
                        else
                        {
                            byte b = reader.ReadByte();
                            var bits = new BitArray(new byte[] { b });
                            encoded.Add(bits);
                            currentDataLenght++;
                            if(dataLenghtBytes == currentDataLenght)
                            {
                                //Make sure to clear everything -> new LookupTable inc.
                                LookupTable.Add(dict);
                                dict = new Dictionary<char, BitArray>();
                                HufmannEncoded.Add(CreateBitArray(encoded));
                                encoded.Clear();
                                creatingLookupTable = true;
                                tmp++;
                                currentDataLenght = 0;
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught in process: {0}", ex);
            }
        }
        private BitArray CreateBitArray(List<BitArray> b)
        {
            //calc the size
            int size = 0;
            for (int i = 0; i < b.Count; i++)
            {
                for (int j = 0; j < b[i].Length; j++)
                {
                    size++;
                }

            }
            BitArray a = new BitArray(size);
            int position = 0;
            for (int i = 0; i < b.Count; i++)
            {
                for (int j = 0; j < b[i].Length; j++)
                {
                    a[position] = b[i][j];
                    position++;
                }

            }
            return a;
        }
    }
}
