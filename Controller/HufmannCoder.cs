﻿using Huffman.Model;
using HufmannRemastered.IO;
using HufmannRemastered.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HufmannRemastered.Controller
{
    public class HufmannCoder
    {
        private MinHeap MinHeap { get; set; }
        public byte[] InputBytes { get; set; }
        private Dictionary<char, int> Frequency { get; set; }
        public Dictionary<char, BitArray> LookupTable { get; set; }
        public List<BitArray> HufmannEncoded { get; set; }
        /// <summary>
        /// in bits
        /// </summary>
        public int CompressedSize { get; set; }
        public int OriginalSize { get; set; }

        public HufmannCoder(byte[] input)
        {
            MinHeap = new MinHeap();
            InputBytes = input;
            OriginalSize = InputBytes.Length * 8;
            HufmannEncoded = new List<BitArray>();
            Frequency = new Dictionary<char, int>();
            CreateFrequency();
            CreateHeap();
            LookupTable = CreateLookupTable(MinHeap.Peek());
            Encode();
        }
        private void Encode()
        {
            foreach (byte b in InputBytes)
            {
                BitArray code = LookupTable[Convert.ToChar(b)];
                HufmannEncoded.Add(code);
                CompressedSize += code.Length;
            }
        }
        private void CreateFrequency()
        {
            foreach(byte b in InputBytes)
            {
                char znak = Convert.ToChar(b);
                if (Frequency.ContainsKey(znak))
                {
                    Frequency[znak]++;
                }
                else
                {
                    Frequency.Add(znak, 1);
                }
            }
        }
        private void CreateHeap()
        {
            //init
            foreach (KeyValuePair<char, int> _freq in Frequency)
            {
                MinHeap.Add(new HufmannNode(_freq.Key, _freq.Value));
            }
            //
            while (MinHeap.Count > 1)
            {
                var low1 = MinHeap.Pop();
                var low2 = MinHeap.Pop();
                HufmannNode n = new HufmannNode('\0', low1.Frequency + low2.Frequency, low1, low2);
                MinHeap.Add(n);
            }
        }
        private Dictionary<char, BitArray> CreateLookupTable(HufmannNode tree)
        {
            Dictionary<char, BitArray> lookupTable = new Dictionary<char, BitArray>();
            TraverseTree(tree, new List<bool>(), ref lookupTable);

            return lookupTable;
        }

        private void TraverseTree(HufmannNode node, List<bool> ba, ref Dictionary<char, BitArray> lookupTable)
        {
            if (node.IsLeaf())
            {
                lookupTable.Add(node.Character, new BitArray(ba.ToArray()));
                return;
            }
            List<bool> bl = new List<bool>(ba);
            List<bool> br = new List<bool>(ba);
            bl.Add(true);
            TraverseTree(node.Left, bl, ref lookupTable);

            br.Add(false);
            TraverseTree(node.Right, br, ref lookupTable);
        }
        public void HufmannFileWriter(string fileName, List<BitArray> encoded, Dictionary<char, BitArray> LookupTable)
        {
            int length = 0;
            for (int i = 0; i < encoded.Count; i++)
            {
                length += encoded[i].Length;
            }
            try
            {
                using (BinaryWriter writer = new BinaryWriter(File.Open(fileName, FileMode.Append)))
                {
                    foreach (KeyValuePair<char, BitArray> item in LookupTable)
                    {
                        var key = item.Key;
                        var value = item.Value;
                        writer.Write(Convert.ToByte(key));
                        writer.Write(Convert.ToByte(value.Length));
                        foreach (bool bit in value)
                        {
                            writer.Write(bit);
                        }
                        writer.Flush();
                    }
                    writer.Write(Convert.ToByte(Helpers.separator));
                    writer.Flush();

                    BitArray bit_array = new BitArray(length);
                    var length_bytes = bit_array.Length / 8 + (bit_array.Length % 8 == 0 ? 0 : 1);

                    writer.Write(length);
                    writer.Flush();

                    int position = 0;
                    for (int i = 0; i < encoded.Count; i++)
                    {
                        for (int j = 0; j < encoded[i].Length; j++)
                        {
                            bit_array[position] = encoded[i][j];
                            position++;
                        }

                    }
                    byte[] bytes = new byte[length_bytes];

                    bit_array.CopyTo(bytes, 0);

                    writer.Write(bytes);
                    writer.Flush();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught in process: {0}", ex);
            }
        }

    }
}
